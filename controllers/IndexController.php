<?php
namespace Controllers;

class IndexController extends _BaseController {
    public function indexAction() {
        return "It's works";
    }

    public function route404Action() {
        $this->response->setStatusCode(404, 'Page not found');
        return 'Not found';
    }
}
