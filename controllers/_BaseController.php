<?php
namespace Controllers;

use Phalcon\Mvc\Dispatcher;
use Sakai\Exceptions\BaseException;

class _BaseController extends \Phalcon\Mvc\Controller {
    public function initialize() {
        $this->view->disable();
        header('Access-Control-Allow-Origin: *');
    }

    public function jsonResponse($callback) {
        try {
            $response = $callback();
            if (is_array($response))
                $this->response->setJsonContent(isset($response['sakai']) ? $response['sakai'] : array_merge(['status' => 'success'], $response));
            else
                $this->response->setContent($response);
        } catch(BaseException $e) {
            $this->response->setJsonContent(array_merge(['status' => 'error', 'message' => $e->getMessage()], ($e->getData() ? $e->getData() : [])));
        }

        return $this->response;
    }
}
