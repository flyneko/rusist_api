<?php
namespace Controllers;

use Components\MimeExtensions;
use Models\Key;
use Sakai\Api;
use Sakai\Exceptions\AccessDeniedException;
use Sakai\Exceptions\InternalException;

class CourseController extends _BaseController {
    public function beforeExecuteRoute($dispatcher) {
        $key = Key::getByKey($this->request->get('key'));

        if (!$key) {
            $this->jsonResponse(function() {
                throw new AccessDeniedException($this->request->get('key'));
            });
            exit($this->response->send());
        }

        if (!$this->request->get('courseId')) {
            $this->jsonResponse(function() {
                throw new InternalException(['data' => 'Empty course id']);
            });
            exit($this->response->send());
        }

    }

    public function modulesAction() {
        return $this->jsonResponse(function() {
            return ['sakai' => Api::modules($this->request->get('courseId'))];
        });
    }

    public function glossaryAction() {
        return $this->jsonResponse(function() {
            return ['sakai' => Api::glossary($this->request->get('courseId'))];
        });
    }

    public function syllabusAction() {
        return $this->jsonResponse(function() {
            return ['sakai' => Api::syllabus($this->request->get('courseId'))];
        });
    }
    
    public function folderAction() {
        return $this->jsonResponse(function() {
            return ['sakai' => Api::resources($this->request->get('courseId'), $this->request->get('folderId'))];
        });
    }

    public function sectionAction() {
        return $this->jsonResponse(function() {
            return Api::section($this->request->get('courseId'), $this->request->get('sectionId'));
        });
    }

    public function resourceAction() {
        $resourceId     = $this->request->get('resourceId');
        $fileName       = array_pop(explode('/', $resourceId));
        $fileExtension  = array_pop(explode('.', $fileName));
        $cacheFile      = ROOT . DS . 'cache_resources' . DS . sha1($resourceId);

        if (!file_exists($cacheFile)) {
            $sakaiFile = Api::content($this->request->get('courseId'), $resourceId);
            file_put_contents($cacheFile, $sakaiFile);
        }
        
        header('Content-Description: File Transfer');
        header('Content-Type: ' . MimeExtensions::getMimeByExtension($fileExtension));
        header('Content-Disposition: attachment; filename=' . sprintf('"%s"', addcslashes($this->request->get('name'), '"\\')));
        header('Content-Transfer-Encoding: binary');
        header('Connection: Keep-Alive');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Length: ' . filesize($cacheFile));
        readfile($cacheFile);
    }
}
