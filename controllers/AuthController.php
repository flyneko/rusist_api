<?php
namespace Controllers;

use Models\Key;
use Models\User;
use Sakai\Exceptions\AccessDeniedException;
use Sakai\Exceptions\BaseException;
use Sakai\Exceptions\InternalException;

class AuthController extends _BaseController {
    public function defaultLoginAction() {
        return $this->jsonResponse(function() {
            if (!filter_var($this->request->get('username'), FILTER_VALIDATE_EMAIL))
                throw new BaseException('INVALID_EMAIL_ADDRESS');

            // Check if the user exist
            $user = User::findFirstByEmail($this->request->get('username'));
            if (!$user || ($user && !$this->security->checkHash($this->request->get('password'), $user->password)))
                throw new BaseException('INVALID_USER_CREDENTIALS');

            return [
                'key'        => $user->Key->ikey,
                'firstname'  => $user->first_name,
                'lastname'   => $user->last_name,
                'middlename' => $user->middle_name,
                'email'      => $user->email
            ];
        });
    }

    public function socialLoginAction() {
        return $this->jsonResponse(function() {
            $key = Key::findFirst([
                'conditions' => 'social_network = ?0 AND social_id = ?1 AND type = "social"',
                'bind' => [$this->request->get('sn'), $this->request->get('id')]
            ]);

            if (!$key) {
                $key                    = new Key();
                $key->type              = 'social';
                $key->social_id         = $this->request->get('id');
                $key->social_network    = $this->request->get('sn');
                $key->social_token      = $this->request->get('token');
                $key->first_name        = $this->request->get('firstname');
                $key->last_name         = $this->request->get('lastname');
                $key->middle_name       = $this->request->get('middlename');

                if (!$key->save())
                    throw new InternalException(['data' => (!empty($key->getMessages()) ? $key->getMessages() : 'Social login error')]);
            }

            return [
                'type'       => $key->type,
                'sn'         => $key->social_network,
                'key'        => $key->ikey,
                'firstname'  => $key->first_name,
                'lastname'   => $key->last_name,
                'middlename' => $key->middle_name,
            ];
        });
    }

    public function sakaiLoginAction() {
        return $this->jsonResponse(function() {
            $sakaiUser = \Sakai\Api::auth($this->request->get('username'), $this->request->get('password'));
            var_dump($sakaiUser);
            $key = Key::findFirst(['conditions' => 'email = ?0 AND type = "sakai"', 'bind' => [$sakaiUser['email']]]);
            if (!$key) {
                $key                    = new Key();
                $key->type              = 'sakai';
                $key->first_name        = $sakaiUser['firstname'];
                $key->last_name         = $sakaiUser['lastname'];
                $key->email             = $sakaiUser['email'];
                $key->sakai_user        = json_encode($sakaiUser['user']);

                if (!$key->save())
                    throw new InternalException(['data' => (!empty($key->getMessages()) ? $key->getMessages() : 'Sakai login error')]);
            }

            $sakaiUser['key']   = $key->ikey;
            $sakaiUser['type']  = $key->type;

            return $sakaiUser;
        });
    }
    
    public function registerAction() {
       return $this->jsonResponse(function() {
           $user                   = new User();
           $user->first_name       = $this->request->get('firstname');
           $user->last_name        = $this->request->get('lastname');
           $user->middle_name      = $this->request->get('middlename');
           $user->email            = $this->request->get('email');
           $user->password         = $this->request->get('password');

           if ($user->save()) {
               $key             = new Key();
               $key->user_id    = $user->id;
               $key->type       = 'default';

               if ($key->save())
                   return [
                       'key'        => $key->ikey,
                       'firstname'  => $user->first_name,
                       'lastname'   => $user->last_name,
                       'middlename' => $user->middle_name,
                       'email'      => $user->email
                   ];
               else {
                   $user->delete();
                   throw new InternalException(['data' => (!empty($key->getMessages()) ? $key->getMessages() : 'Registration error')]);
               }
           } else {
               throw new InternalException(['data' => (!empty($user->getMessages()) ? $user->getMessages() : 'Registration error')]);
           }
       });
    }

    public function changePasswordAction() {
        return $this->jsonResponse(function() {
            $key = Key::getByKey($this->request->get('key'));
            
            if (!$key)
                throw new AccessDeniedException($this->request->get('key'));
            
            if ($key->User->update(['password' => $this->security->hash($this->request->get('password'))]))
                return [];
            else
                throw new InternalException(['data' => 'Change password error']);
        });
    }
    
    public function checkKeyAction() {
        return $this->jsonResponse(function() {
            $key = Key::getByKey($this->request->get('key'));

            if (!$key)
                throw new AccessDeniedException($this->request->get('key'));
            
            $type = $key->type;
            
            if (!empty($key->social_network))
                $type = $key->social_network;

            if (!empty($key->email) || $key->User)
                $userName = !empty($key->email) ? $key->email : $key->User->email;

            if (!empty($key->social_id))
                $userName = $key->social_id;
            
            if ($key->type == 'sakai')
                $userName = $key->username;
            
            if ($key->User)
                $key->email = $key->User->email;
            
            return [
                'username'  => $userName,
                'type'      => $type,
                'email'     => $key->email,
                'user'      => $key->sakai_user
            ];
        });
    }
}
