<?php
namespace Frontend\Controllers;

use Models\Category;
use Models\GeoCity;
use Models\GeoRegion;
use Phalcon\Mvc\View;

class AjaxController extends _ControllerBase {

    public function beforeExecuteRoute($dispatcher) {
        $this->view->disable();
        parent::beforeExecuteRoute($dispatcher);
    }

    public function categoriesAction() {
        return $this->ajaxRequest(function() {
            $query = $this->request->getQuery('query', 'string');
            $categories = Category::find([
                'conditions'    => 'type = :type: AND name LIKE :name: AND id NOT IN (SELECT parent_id FROM Models\Category GROUP BY parent_id)',
                'bind'          => ['name' => '%' . $query . '%', 'type' => 'material'],
                'order'         => 'parent_id',
                'limit'         => 20
            ]);

            $response = ['query' => $query, 'suggestions' => []];

            foreach ($categories as $category)
                $response['suggestions'][] = [
                    'value' => $category->name,
                    'data'  => !$category->Parent ? ['id' => $category->id] : ['id' => $category->id, 'parent' => $category->Parent->name]
                ];

            return $response;
        }, false);
    }

    public function setTimeZoneAction() {
        return $this->ajaxRequest(function() {
            if ($this->cookies->set('timezone', $this->dispatcher->getParam('offset'), strtotime('+1 year')))
                return ['type' => 'success'];
        }, false);
    }

    public function geoSearchAction() {
        return $this->ajaxRequest(function() {
            $response   = [];
            $limit      = 10;
            $cities     = GeoCity::find([
                'conditions'    => 'name LIKE :name: AND country_id = :country:',
                'bind'          => ['name' => '%' . $this->request->getQuery('q') . '%', 'country' => $this->auth->getCountry('id')],
                'limit'         => $limit
            ]);

            if ($this->dispatcher->getParam('type') == 'full') {
                $regions = GeoRegion::find([
                    'conditions'    => 'name LIKE :name: AND country_id = :country:',
                    'bind'          => ['name' => '%' . $this->request->getQuery('q') . '%', 'country' => $this->auth->getCountry('id')],
                    'limit'         => $limit
                ]);

                foreach ($cities as $city)
                    $response[] = ['id' => 'city_' . $city->id, 'text' => $city->name . ' (' . (!empty($city->Region->name) ? $city->Region->name : '') . ')'];
                
                foreach ($regions as $region)
                    $response[] = ['id' => 'region_' . $region->id, 'text' => $region->name ];
            }

            if ($this->dispatcher->getParam('type') == 'city') {
                foreach ($cities as $item)
                    $response[] = ['id' => $item->id, 'text' => $item->name . ' (' . (!empty($item->Region->name) ? $item->Region->name : '') . ')'];
            }

            return $response;
        }, false);
    }
}

