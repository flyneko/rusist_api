<?php
try {
    ini_set('display_errors', 'Off');

    require_once __DIR__ . '/bootstrap/Constants.php';
    require ROOT . DS . 'bootstrap' . DS . 'Loader.php';
    require ROOT . DS . 'bootstrap' . DS . 'Services.php';
    require ROOT . DS . 'bootstrap' . DS . 'Debugger.php';
    require ROOT . DS . 'bootstrap' . DS . 'Bootstrap.php';

    $app = new \Phalcon\Mvc\Application(\Bootstrap\Services::init());

    // Init system php components and other
    \Bootstrap\Bootstrap::init();

    // Load controllers, models, components etc.
    Loader::init();
    
    echo $app->handle()->getContent();
} catch (Exception $e) {
    echo $e->getMessage();
    //echo $e->getTraceAsString();
}