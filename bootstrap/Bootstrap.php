<?php
namespace Bootstrap;

class Bootstrap {
    public static function init() {
        date_default_timezone_set('Europe/Moscow');
        mb_internal_encoding("UTF-8");

        \Phalcon\Mvc\Model::setup([
            'notNullValidations' => false
        ]);
        
        if (DEBUG_MODE) {
            error_reporting(E_ERROR | E_COMPILE_ERROR | E_PARSE);
            ini_set('display_errors', 'On');
            (new \Phalcon\Debug)->listen();
        }
    }
}