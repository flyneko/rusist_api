<?php
namespace Bootstrap;

use Phalcon\Mvc\User\Component;
use Phalcon\Logger\Adapter\File as FileLogger;

class Debugger extends Component {
    private static $_instance = null;

    private function __constructor() {}

    private static function getInstance() {
        if (empty(self::$_instance))
            self::$_instance = new self();
        return self::$_instance;
    }

    public static function init() {
        $obj = self::getInstance();
        $eventsManager = $obj->eventsManager;
        $config = $obj->config->application;

        $obj->eventsManager->attach("dispatch", function($event, $dispatcher) use ($config) {
            $logger = new FileLogger(APP_PATH . $config->logsDir . "controllers.log");
            if ($event->getType() == 'beforeDispatch') {
                $logger->log($dispatcher->getActiveController(), \Phalcon\Logger::INFO);
            }
        });
        // $obj->dispatcher->setEventsManager($eventsManager);

        $eventsManager->attach('db', function ($event, $connection) use ($config) {
            $logger = new FileLogger(APP_PATH . $config->logsDir . "db.log");

            if ($event->getType() == 'beforeQuery') {
                $logger->log($connection->getSQLStatement(), \Phalcon\Logger::INFO);
            }
        });
        $obj->db->setEventsManager($eventsManager);

        $eventsManager->attach('loader', function($event, $loader) use ($config) {
            $logger = new FileLogger(APP_PATH . $config->logsDir . "loader.log");

            if ($event->getType() == 'beforeCheckPath') {
                $logger->log($loader->getCheckedPath(), \Phalcon\Logger::INFO);
            }
        });
        $obj->loader->setEventsManager($eventsManager);

    }
}
