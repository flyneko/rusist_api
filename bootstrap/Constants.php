<?php
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', realpath(__DIR__ . DS . '..'));
define('CACHE', ROOT . DS . 'cache');
define('CONFIGS', ROOT . DS . 'configs');
define('COMPONENTS', ROOT . DS . 'components');
