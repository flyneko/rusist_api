<?php
class Loader extends \Phalcon\Mvc\User\Component {
    public static function init() {
        $obj = new self();
        $loader = $obj->di->get("loader");
        $loader->registerNamespaces([
            'Models'        => ROOT . DS . 'models',
            'Library'       => ROOT . DS . 'library',
            'Components'    => ROOT . DS . 'components',
            'Controllers'   => ROOT . DS . 'controllers',

            'Ramsey\Uuid'   => ROOT . DS . 'library' . DS . 'Uuid',
            'Sakai'         => ROOT . DS . 'library' . DS . 'Sakai',

        ]);
        $loader->register();
    }
}
