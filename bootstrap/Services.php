<?php
namespace Bootstrap;

use \Phalcon\DI\FactoryDefault;
use \Phalcon\Mvc\View;
use \Phalcon\Mvc\Url;
use \Phalcon\Mvc\Dispatcher;
use \Phalcon\Db\Adapter\Pdo\Mysql;
use \Phalcon\Mvc\View\Engine\Volt;
use \Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use \Phalcon\Events\Manager as EventsManager;
use \Phalcon\Loader;
class Services {
    public static function init() {
        // Read configuration
        $config = [];
        foreach (glob(CONFIGS . DS . '*.yml') as $item)
            $config[array_shift(explode('.', basename($item)))] = (new \Phalcon\Config\Adapter\Yaml($item))->toArray();

        $config = new \Phalcon\Config($config);
        define('SUBDIRECTORY', $config->application->subdirectory);
        define('DEBUG_MODE', $config->application->debug);

        $di = new FactoryDefault();
        
        $di->setShared('db', new Mysql($config->get('database')->toArray()));
        $di->setShared('config', $config);
        $di->setShared('eventsManager', new EventsManager());
        $di->set('loader', new Loader());
        $di->setShared('modelsManager', new \Phalcon\Mvc\Model\Manager());
        $di->setShared('modelsMetadata', new MetaDataAdapter());

        // Dispatcher
        $di->setShared('dispatcher', function() use ($config, $di) {
            $dispatcher = new Dispatcher();
            $dispatcher->setDefaultNamespace('Controllers');
            return $dispatcher;
        });

        // View render engine definition
        $di->setShared('view', new View());

        // Crypt
        $di->set('crypt', function() use ($config) {
            $crypt = new \Phalcon\Crypt();
            $crypt->setKey('jfk423sdfkl4#32()djIUR8&*IOJNRo453');
            return $crypt;
        });
        
        // Router
        $di->setShared('router', function() use ($config) {
            $yaml = new \Library\YamlRouter(null, [false]);
            $router = $yaml->build(CONFIGS . DS . 'routes.yml');
            $router->removeExtraSlashes(true);

            $router->notFound([
                "controller" => "index",
                "action"     => "route404",
            ]);

            return $router;
        });
        return $di;
    }
}