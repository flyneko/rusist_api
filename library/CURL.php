<?php
namespace Library;

class CURL {
    public $info;
    public $error;
    public $options;
    public $content;
    public $http_code;
    public $url;
    
    private static $_info;
    
    private static function _request($params = []) {
        $curl = curl_init();
        $defaultParams = [
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_TIMEOUT         => 30,
            CURLOPT_CONNECTTIMEOUT  => 30,
            CURLOPT_SSL_VERIFYHOST  => false,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_IPRESOLVE       => CURL_IPRESOLVE_V4,
            CURLOPT_ENCODING        => '',
        ];

        curl_setopt_array($curl, $defaultParams);
        curl_setopt_array($curl, $params);

        $content = trim(curl_exec($curl));

        $info = curl_getinfo($curl);
        $error = curl_error($curl);

        curl_close($curl);
        $curl = null;
        unset($curl);
        
        return [
            'content'   => $content,
            'error'     => $error,
            'info'      => $info,
            'options'   => $params,
            'http_code' => (int)$info['http_code'],
            'url'       => $info['url']
        ];
    }

    public function __construct($url = '', $params = []) {
        if (!empty($url))
            $params[CURLOPT_URL] = $url;

        $request = self::_request($params);

        $this->error        = $request['error'];
        $this->info         = $request['info'];
        $this->options      = $request['options'];
        $this->content      = $request['content'];
        $this->http_code    = $request['http_code'];
        $this->url          = $request['url'];
    }

    public static function execute($url = '', $params = []) {
        if (!empty($url))
            $params[CURLOPT_URL] = $url;
        $request = self::_request($params);
        $content = $request['content'];
        unset($request['content']);
        self::$_info = $request;
        return $content;
    }
}