<?php
namespace Library;

class YamlRouter {
    private $_router;
    
    public function __construct($className = '', $args = []) {
        $objectClassName = '\Phalcon\Mvc\Router' . (empty($className) ? $className : '\\' . $className);
        $object = (new \ReflectionClass($objectClassName))->newInstanceArgs($args);
        $this->_router = $object;
    }

    public function build($config = null) {
        if (empty($config))
            return $this->_router;
        
        $routes = (new \Phalcon\Config\Adapter\Yaml($config))->toArray();

        foreach ($routes as $name => $value) {
            $names = explode('_', $name);
            $value['url'] = isset($value['url'])
                ? $value['url'] : ('/'.str_replace('_', '/', $name));
            // normalized url
            $length = strlen($value['url']);
            if ($value['url'][ $length - 1 ] === '/'
                && $length > 1 && $this->_removeExtraSlashes) {
                $value['url'] = rtrim($value['url'], '/');
            }
            $value['controller'] = isset($value['controller']) ? $value['controller'] : $names[0];
            if (isset($value['action'])) {
            } elseif (isset($names[1])) {
                $value['action'] = $names[1];
            } else {
                $value['action'] = 'index';
            }
            $method = isset($value['method']) ? $value['method'] : 'GET';
            $this->_router->add($value['url'], $value)->setName($name)->via($method);
        }

        return $this->_router;
    }
}