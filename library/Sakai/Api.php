<?php
namespace Sakai;

use Library\CURL;
use Sakai\Exceptions\BaseException;
use Sakai\Exceptions\InternalException;

class Api {
    const URL = 'http://rusist.softneva.ru/rusist-tool/export';
    
    private static function _request($entity, $data = [], $file = false) {
        $url = self::URL . '?tool=' . $entity  . (!empty($data) ?  '&' . http_build_query($data) : '');
        $curl = new CURL($url);

        if ($file)
            return $curl->content;

        $json = json_decode($curl->content, true);

        if (empty($curl->content) || empty($json))
            throw new InternalException(['data' => 'Empty response']);

        $response = !empty($json) ? $json : ['response' => $curl->content];
        
        if (!empty($json) && $json['status'] == 'error')
            throw new BaseException($response['message']);
        
        return $response;
    }
    
    public static function auth($user, $password) {
        return self::_request('auth', ['user' => $user, 'pass' => $password]);
    }
    
    public static function modules($courseId) {
        return self::_request('modules', ['siteId' => $courseId]);
    }

    public static function resources($courseId, $folderId = '') {
        return self::_request('resources', ['siteId' => $courseId, 'folderId' => $folderId]);
    }


    public static function glossary($courseId) {
        return self::_request('glossary', ['siteId' => $courseId]);
    }

    public static function syllabus($courseId) {
        return self::_request('syllabus', ['siteId' => $courseId]);
    }


    public static function section($courseId, $sectionId = '') {
        $id = explode('-', $sectionId);
        $moduleId = $id[0];
        $sectionId = $id[1];
        return self::_request('section', ['siteId' => $courseId, 'sectionId' => $sectionId, 'moduleId' => $moduleId]);
    }
    
    public static function content($courseId, $resourceId) {
        return self::_request('content', ['siteId' => $courseId, 'resourceId' => $resourceId], true);
    }
}