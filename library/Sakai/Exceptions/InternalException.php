<?php
namespace Sakai\Exceptions;

class InternalException extends BaseException {
    public function __construct($data = false) {
        parent::__construct('INTERNAL_ERROR', $data);
    }
}