<?php
namespace Sakai\Exceptions;

class AccessDeniedException extends BaseException {
    public function __construct($key = '') {
        parent::__construct('ACCESS_DENIED', ['data' => $key]);
    }
}