<?php
namespace Sakai\Exceptions;

class BaseException extends \Exception {
    private $_data = false;

    public function __construct($message, $data = false) {
        parent::__construct($message, 0, null);

        $this->_data = $data;
    }

    public function getData() { return $this->_data; }
}