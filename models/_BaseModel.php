<?php
namespace Models;

class _BaseModel extends \Phalcon\Mvc\Model {
    public static $cache = false;

    public function getMessages() {
        $messages = [];
        foreach (parent::getMessages() as $message)
            $messages[] = $message->getMessage();
        return $messages;
    }
    
    protected static function _createKey($parameters) {
        $uniqueKey = [];

        foreach ($parameters as $key => $value) {
            if (is_scalar($value))
                $uniqueKey[] = $key . ':' . $value;
            else {
                if (is_array($value))
                    $uniqueKey[] = $key . ':[' . self::_createKey($value) .']';
            }
        }
        return md5(get_called_class() . join(',', $uniqueKey));
    }

    public function setSource($source) {
        $source = $this->getDI()->get('config')->database->prefix . $source;
        parent::setSource($source);
    }
    
    public static function getTable() {
        $object = new static();
        return $object->getSource();
    }

    public static function find($parameters = []) {
        // Convert the parameters to an array
        if (!is_array($parameters))
            $parameters = [$parameters];

        if (isset($parameters['pager'])) {
            $parameters['limit'] = $parameters['pager']['per_page'];
            $parameters['offset'] = ($parameters['pager']['page'] - 1) * $parameters['pager']['per_page'];
        }

        // Check if a cache key wasn't passed
        // and create the cache parameters
        if (isset($parameters["cache"])) {

            $parameters["cache"] = [
                "key"      => self::_createKey($parameters),
                "lifetime" => strtotime('+' . isset($parameters["cache"]) ? $parameters["cache"] : static::$cache) - time(),
            ];
        }

        return parent::find($parameters);
    }
}