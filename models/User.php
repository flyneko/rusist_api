<?php
namespace Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator;

class User extends _BaseModel {
    public $id;
    public $email;
    public $password;
    public $first_name;
    public $last_name;
    public $middle_name = '';
    
    public function beforeValidationOnCreate() {
        $this->password = $this->getDI()->get('security')->hash($this->password);
    }

    public function validation() {
        $validator = new Validation();

        $validator->add("email", new Validator\Email(['message' => 'Invalid e-mail address format']));
        $validator->add("email", new Validator\Uniqueness(['message' => 'Input e-mail already existing']));
        
        return $this->validate($validator);
    }

    public function initialize() {
        $this->setSource("users");
        $this->hasOne('id', 'Models\Key', 'user_id', ['alias' => 'Key']);
    }
}
