<?php
namespace Models;

use Phalcon\Validation;
use Phalcon\Validation\Validator;
use Ramsey\Uuid\Uuid;

class Key extends _BaseModel {
    public $ikey;
    public $type;
    public $email           = '';
    public $first_name      = '';
    public $last_name       = '';
    public $middle_name     = '';
    public $social_network  = '';
    public $social_token    = '';
    public $social_id       = '';
    public $sakai_user      = '';
    public $user_id         = '';

    public function beforeValidationOnCreate() {
        $this->ikey = Uuid::uuid4()->toString();
    }
   
    public function afterFetch() {
        $this->sakai_user = json_decode($this->sakai_user);
    }

    public function initialize() {
        $this->setSource("keys");
        $this->belongsTo('user_id', 'Models\User', 'id', ['alias' => 'User']);
    }
    
    public static function getByKey($key = '') {
        $key = self::findFirstByIkey($key);
        return $key ? $key : false;
    }
}
