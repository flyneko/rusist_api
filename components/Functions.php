<?php
namespace Components;

final class Functions {
    public static function generateTimeRange() {
        $timeRange = [];
        $timeRangeStart = strtotime('00:00');
        $timeRangeEnd = strtotime('23:30');
        while ($timeRangeStart <= $timeRangeEnd) {
            $timeRange[] = date('H:i', $timeRangeStart);
            $timeRangeStart = strtotime('+30 minutes', $timeRangeStart);
        }
        $timeRange[] = '23:59';
        return $timeRange;
    }

    public static function getNumbers($num, $strict = false) {
        return !$strict ? (string)preg_replace('#[^\d\.]#u', '', $num) : (int)preg_replace('#[^\d]#u', '', $num);
    }
    
    public static function numberWordDeclination($number, $suffix) {
        $keys = array(2, 0, 1, 1, 1, 2);
        $mod = $number % 100;
        $suffix_key = ($mod > 7 && $mod < 20) ? 2: $keys[min($mod % 10, 5)];
        return $suffix[$suffix_key];
    }


    public static function humanDate($timestamp, $options = []) {
        $monthes = ['января', 'ферваля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];

        $difference = time() - $timestamp;

        if (isset($options['strict'])) {
            if ($difference / 3600 > 1 && $difference / 3600 < 23)
                return 'Сегодня';

            if ($difference / 3600 > 23 && $difference / 3600 < 47)
                return 'Вчера';
            
            return (int)date('d', $timestamp) . ' ' . $monthes[date('m', $timestamp) - 1] . (date('Y', $timestamp) != date('Y') ? ' ' . date('Y', $timestamp) : '');
        }
        
        if ($difference <= 59)
            return $difference . ' ' . self::numberWordDeclination($difference, ['cекунду', 'секунды', 'секунд']) . ' назад';

        if ($difference / 60 > 1 && $difference / 60 < 59)
            return round($difference / 60) . ' ' . self::numberWordDeclination(round($difference / 60), ['минуту', 'минуты', 'минут']) . ' назад';

        if ($difference / 3600 > 1 && $difference / 3600 < 23)
            return round($difference / 3600) . ' ' . self::numberWordDeclination(round($difference / 3600), ['час', 'часа', 'часов']) . ' назад';

        return (int)date('d', $timestamp) . ' ' . $monthes[date('m', $timestamp) - 1] . (isset($options['year']) ? ' ' . date('Y', $timestamp) : '') . (isset($options['time']) ? ' в ' . date('H:i', $timestamp) : '');
    }

    public static function getTimeZones() {
        return [
            '-720'  => '(GMT -12:00) Эневеток, Кваджалейн',
            '-660'  => '(GMT -11:00) Остров Мидуэй, Самоа',
            '-600'  => '(GMT -10:00) Гавайи',
            '-540'  => '(GMT -9:00) Аляска',
            '-480'  => '(GMT -8:00) Тихоокеанское время (США и Канада), Тихуана',
            '-420'  => '(GMT -7:00) Горное время (США и Канада), Аризона',
            '-360'  => '(GMT -6:00) Центральное время (США и Канада), Мехико',
            '-300'  => '(GMT -5:00) Восточное время (США и Канада), Богота, Лима',
            '-270'  => '(GMT -4:30) Каракас',
            '-240'  => '(GMT -4:00) Атлантическое время (Канада), Ла Пас',
            '-210'  => '(GMT -3:30) Ньюфаундленд',
            '-180'  => '(GMT -3:00) Бразилия, Буэнос-Айрес, Джорджтаун',
            '-120'  => '(GMT -2:00) Среднеатлантическое время',
            '-60'   => '(GMT -1:00) Азорские острова, острова Зелёного Мыса',
            '0'     => '(GMT) Дублин, Лондон, Лиссабон, Касабланка, Эдинбург',
            '60'    => '(GMT +1:00) Брюссель, Копенгаген, Мадрид, Париж, Берлин',
            '120'   => '(GMT +2:00) Афины, Бухарест, Рига, Таллин',
            '180'   => '(GMT +3:00) Москва, Минск, Санкт-Петербург, Киев, Волгоград',
            '210'   => '(GMT +3:30) Тегеран',
            '240'   => '(GMT +4:00) Абу-Даби, Баку, Тбилиси, Ереван',
            '270'   => '(GMT +4:30) Кабул',
            '300'   => '(GMT +5:00) Екатеринбург, Исламабад, Карачи, Ташкент',
            '330'   => '(GMT +5:30) Мумбай, Колката, Ченнаи, Нью-Дели',
            '345'   => '(GMT +5:45) Катманду',
            '360'   => '(GMT +6:00) Омск, Новосибирск, Алма-Ата, Астана',
            '390'   => '(GMT +6:30) Янгон, Кокосовые острова',
            '420'   => '(GMT +7:00) Красноярск, Норильск, Бангкок, Ханой, Джакарта',
            '480'   => '(GMT +8:00) Иркутск, Пекин, Перт, Сингапур, Гонконг',
            '540'   => '(GMT +9:00) Якутск, Токио, Сеул, Осака, Саппоро',
            '570'   => '(GMT +9:30) Аделаида, Дарвин',
            '600'   => '(GMT +10:00) Владивосток, Восточная Австралия, Гуам',
            '660'   => '(GMT +11:00) Магадан, Сахалин, Соломоновы Острова',
            '720'   => '(GMT +12:00) Камчатка, Окленд, Уэллингтон, Фиджи',
        ];
    }
}